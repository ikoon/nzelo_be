/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      AOS.init();
      //alert("I'm alive!");
      //$('#lightgallery').lightGallery();
      $('.grid').imagesLoaded(function () {
        $('.grid').masonry({
          itemSelector: '.grid-item',
          percentPosition: false,
          gutter: 30,
          fitWidth: false
        });
      });

      $(".close-button").click(function() {
        $(this).parent().parent().parent().css('top','-200vh');
      });

      $(".button-self").click(function() {
        $(this).next().css('top','0');
      });

      // $('.path-frontpage figcaption h2').each(function(){
      //   var $this = $(this);
      //   var sin = $this.html();
      //   if (sin.length<1) return;
      //   var sout = sin.substring(0, sin.length-1)+'<span class=offront>'+sin.charAt(sin.length-1)+'</span>';
      //   $this.html(sout);
      // });

      // $('.paragraph--type--text-image h3').each(function(){
      //   var $this = $(this);
      //   var sin = $this.html();
      //   if (sin.length<1) return;
      //   var soutp = sin.substring(0, sin.length-1)+'<span class=of>'+sin.charAt(sin.length-1)+'</span>';
      //   $this.html(soutp);
      // });

      // $('.taxo-type-page h1 .field-item').each(function(){
      //   var $this = $(this);
      //   var sin = $this.html();
      //   if (sin.length<1) return;
      //   var soutt = sin.substring(0, sin.length-1)+'<span class=of>'+sin.charAt(sin.length-1)+'</span>';
      //   $this.html(soutt);
      // });

      // $('.page.full h1').each(function(){
      //   var $this = $(this);
      //   var sin = $this.html();
      //   if (sin.length<1) return;
      //   var soutpf = sin.substring(0, sin.length-1)+'<span class=of>'+sin.charAt(sin.length-1)+'</span>';
      //   $this.html(soutpf);
      // });

      // $('.viewsreference--view-title').each(function(){
      //   var $this = $(this);
      //   var sin = $this.html();
      //   if (sin.length<2) return;
      //   var soutv = sin.substring(0, sin.length-2)+'<span class=of>'+sin.charAt(sin.length-2)+'</span>';
      //   $this.html(soutv);
      // });

    }
  };

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.cookieSettings = {
    attach: function (context, settings) {
      $('span[data-action="cookie_settings"]', context).click(function() {
        $('.eu-cookie-withdraw-tab').click();
      });
      $(function(){
        $('#sliding-popup').css({bottom: - $('#sliding-popup').height()});
      });
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifSlider').each(function (){
                var slides = new Swiper (this, {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: $('.swiper-pagination', this),
                        type: 'bullets',
                        clickable: true,
                    },
                });
            });

          $(context).find('.taxo-images-slider.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              effect: 'fade',
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
            });
          });

          $(context).find('.page-images-slider.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              effect: 'fade',
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              }
            });
          });

          $(context).find('.slider-text-swiper.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              effect: 'fade',
              navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
              },
            });
          });

          $(context).find('.gallery-slider.swiper-container').once('ifSlider').each(function (){
            var slides = new Swiper (this, {
              autoplay: false,
              loop: true,
              effect: 'slide',
              autoHeight: 'true',
              navigation: {
                nextEl: $(this).parent().find('.swiper-button-next.galbut'),
                prevEl: $(this).parent().find('.swiper-button-prev.galbut'),
              },
            });
          });
        }
    };

    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };


  Drupal.behaviors.scrollTo = {
    attach: function (context, settings) {
      $('.scroll-line').on('click', function(){
        $('html,body').animate({
            scrollTop: $('article.home .view-mode-full').offset().top},
          'slow');
      });
    }
  };

})(jQuery, Drupal);
