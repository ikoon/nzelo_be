{
    "name": "drupal/recommended-project",
    "description": "Project template for Drupal 9 projects with a relocated document root",
    "type": "project",
    "license": "GPL-2.0-or-later",
    "homepage": "https://www.drupal.org/project/drupal",
    "support": {
        "docs": "https://www.drupal.org/docs/user_guide/en/index.html",
        "chat": "https://www.drupal.org/node/314178"
    },
    "repositories": [
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "package",
            "package": {
                "name": "google/charts",
                "version": "45",
                "type": "drupal-library",
                "extra": {
                    "installer-name": "google_charts"
                },
                "dist": {
                    "url": "https://www.gstatic.com/charts/loader.js",
                    "type": "file"
                },
                "require": {
                    "composer/installers": "~1.0"
                }
            }
        }
    ],
    "require": {
        "composer/installers": "^1.9",
        "cweagans/composer-patches": "^1.7",
        "drupal/admin_toolbar": " ^2.5",
        "drupal/adminimal_admin_toolbar": " ^1.8",
        "drupal/ajax_loader": " ^2.0",
        "drupal/antibot": " ^1.2",
        "drupal/block_content_permissions": " ^1.7",
        "drupal/captcha": " ^1.0",
        "drupal/charts": " ^3.0",
        "drupal/core-composer-scaffold": "^9.2",
        "drupal/core-project-message": "^9.2",
        "drupal/core-recommended": "^9.2",
        "drupal/crop": " ^2.1",
        "drupal/ctools": " ^3.4",
        "drupal/default_content": " ^1.0",
        "drupal/devel": "^4.1",
        "drupal/draggable_dashboard": " ^1.1",
        "drupal/editor_advanced_link": "^2.0",
        "drupal/entity_reference_revisions": " ^1.8",
        "drupal/eu_cookie_compliance": " 1.9",
        "drupal/field_group": " ^3.0",
        "drupal/field_permissions": " ^1.0",
        "drupal/fontawesome": " ^2.8",
        "drupal/fontawesome_menu_icons": " ^1.4",
        "drupal/geolocation": "^3.7",
        "drupal/google_analytics": " ^3.0",
        "drupal/google_analytics_reports": " ^3.0",
        "drupal/honeypot": " ^2.0",
        "drupal/image_widget_crop": " ^2.3",
        "drupal/imagick": " ^1.2",
        "drupal/libraries": " ^3.0",
        "drupal/linkicon": " ^1.4",
        "drupal/masquerade": " ^2.0",
        "drupal/maxlength": " ^1.0",
        "drupal/menu_admin_per_menu": " ^1.0",
        "drupal/menu_block": " ^1.5",
        "drupal/menu_link_attributes": " ^1.0",
        "drupal/metatag": " ^1.15",
        "drupal/paragraphs": " ^1.11",
        "drupal/pathauto": " ^1.8",
        "drupal/recaptcha": " ^3.0",
        "drupal/redirect_after_login": " ^2.3",
        "drupal/simple_sitemap": " ^3.3",
        "drupal/simplify": "^1.2",
        "drupal/swiftmailer": " ^2.0",
        "drupal/taxonomy_menu_ui": " ^1.4",
        "drupal/token": " ^1.9",
        "drupal/token_filter": " ^1.1",
        "drupal/toolbar_anti_flicker": "^9.3",
        "drupal/toolbar_menu": " ^2.1",
        "drupal/toolbar_menu_clean": " ^1.0",
        "drupal/twig_tweak": " ^2.9",
        "drupal/video_embed_field": " ^2.3",
        "drupal/viewsreference": " ^2.0",
        "drupal/webform": " ^6.0",
        "drupal/webform_views": " ^5.0",
        "drupal/zurb_foundation": " ^6.0",
        "drush/drush": "^10.6",
        "wikimedia/composer-merge-plugin": "^2.0"
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "config": {
        "sort-packages": true
    },
    "extra": {
        "drupal-scaffold": {
            "locations": {
                "web-root": "web/"
            }
        },
        "installer-paths": {
            "web/core": [
                "type:drupal-core"
            ],
            "web/libraries/{$name}": [
                "type:drupal-library"
            ],
            "web/modules/contrib/{$name}": [
                "type:drupal-module"
            ],
            "web/profiles/contrib/{$name}": [
                "type:drupal-profile"
            ],
            "web/themes/contrib/{$name}": [
                "type:drupal-theme"
            ],
            "drush/Commands/contrib/{$name}": [
                "type:drupal-drush"
            ],
            "web/modules/custom/{$name}": [
                "type:drupal-custom-module"
            ],
            "web/profiles/custom/{$name}": [
                "type:drupal-custom-profile"
            ],
            "web/themes/custom/{$name}": [
                "type:drupal-custom-theme"
            ]
        },
        "drupal-core-project-message": {
            "include-keys": [
                "homepage",
                "support"
            ],
            "post-create-project-cmd-message": [
                "<bg=blue;fg=white>                                                         </>",
                "<bg=blue;fg=white>  Congratulations, you’ve installed the Drupal codebase  </>",
                "<bg=blue;fg=white>  from the drupal/recommended-project template!          </>",
                "<bg=blue;fg=white>                                                         </>",
                "",
                "<bg=yellow;fg=black>Next steps</>:",
                "  * Install the site: https://www.drupal.org/docs/8/install",
                "  * Read the user guide: https://www.drupal.org/docs/user_guide/en/index.html",
                "  * Get support: https://www.drupal.org/support",
                "  * Get involved with the Drupal community:",
                "      https://www.drupal.org/getting-involved",
                "  * Remove the plugin that prints this message:",
                "      composer remove drupal/core-project-message"
            ]
        },
        "patches": {
            "drupal/google_analytics": {
                "Add JS-function / method to set ga-disable-...": "https://www.drupal.org/files/issues/2020-08-27/google_analytics-eu_cookie_compliance_support-2917905-53.patch"
            },
            "drupal/fontawesome_menu_icons": {
                "Span tag interferes with link hover": "https://www.drupal.org/files/issues/2020-11-24/fontawesome_menu_icons-span-tag-causes-hover-to-flicker-3184694-2.patch"
            },
            "drupal/redirect_after_login": {
                "Headers have already been sent after upgrade to Drupal 9.2 (can't login)": "https://www.drupal.org/files/issues/2021-06-20/3214949.patch"
            }
        },
        "merge-plugin": {
            "include": [
                "web/modules/contrib/webform/composer.libraries.json",
                "web/modules/contrib/masonry/composer.libraries.json"
            ]
        }
    }
}
